﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain
{
    public class SignalValue
    {
        public int Id { get; set; }
        public float Value { get; set; }
        public bool Valid { get; set; }
        public OutageSignal OutageSignal { get; set; }
    }
}
