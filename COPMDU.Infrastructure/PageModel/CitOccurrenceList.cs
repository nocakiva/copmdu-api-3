﻿using COPMDU.Infrastructure.ClassAttribute;
using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Infrastructure.PageModel
{
    public class CitOccurrenceList
    {

        [PageResultListProperty(@"(<tr>\n<td align=""center"">[\w\W]+?</tr>)")]
        public List<CitOccurrence> Occurrences { get; set; }
    }
}
