using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace COPMDU.Infrastructure.Util
{
    public class Messager
    {

        private readonly IConfiguration _config;

        private string Host => _config["Smtp:Host"];
        private string Port => _config["Smtp:Port"];
        private string Username => _config["Smtp:User"];
        private string Password => _config["Smtp:Password"];

        public Messager(IConfiguration config) 
        {
            _config = config;
        }

        /// <summary>
        /// Envia uma mensagem 
        /// </summary>
        public void Send(string address, string subject, string body)
        {
            var client = new SmtpClient(Host)
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(Username, Password),
                Port = int.Parse(Port),
                EnableSsl = true
            };

            var mailMessage = new MailMessage
            {
                From = new MailAddress(Username),
                Body = body,
                Subject = subject,
            };
            mailMessage.To.Add(address);
            client.Send(mailMessage);

        }


    }
}
