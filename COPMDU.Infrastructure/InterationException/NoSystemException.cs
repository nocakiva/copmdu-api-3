﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Infrastructure.InterationException
{

    [Serializable]
    public class NoSystemException : Exception
    {
        public NoSystemException(int cityId) : base($"Não encontrado um sistema que tenha a cidade de id: {cityId}") { }
        protected NoSystemException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
