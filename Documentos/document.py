import urllib.request as request
import json
import uuid
import pymysql
import re
import pandas as pd

from IPython.core.display import display, HTML


class RenderJSON(object):
    def __init__(self, json_data):
        if isinstance(json_data, dict):
            self.json_str = json.dumps(json_data)
        else:
            self.json_str = json_data
        self.uuid = str(uuid.uuid4())
        
    def _ipython_display_(self):
        display(HTML('<div id="{}" style="height: auto; width:100%;"></div>'.format(self.uuid)))
        display(HTML("""<script>
        require(["https://rawgit.com/caldwell/renderjson/master/renderjson.js"], function() {
          renderjson.set_show_to_level(1)
          document.getElementById('%s').appendChild(renderjson(%s))
        });</script>
        """ % (self.uuid, self.json_str)))


def httpGet(url) :
    with request.urlopen(url) as page:
        return page.read().decode('utf-8')

def pretty(jsonString) :
    return RenderJSON(jsonString)

def getSettings() :
    with open("../COPMDU.Api/appsettings.json", 'r', encoding=('utf-8-sig')) as settings_file:
        return json.load(settings_file)

settings = getSettings()


connStr = settings['Connection']['CopMdu']
def getCursor() :
    db = pymysql.connect(
            re.search("Server=(.+?);", connStr).group(1), 
            re.search("Uid=(.+?);", connStr).group(1), 
            re.search("Pwd=(.+?);", connStr).group(1), 
            re.search("Database=(.+?);", connStr).group(1),
            cursorclass=pymysql.cursors.DictCursor)

    return db.cursor()

def sample(table) :
    cursor = getCursor()
    cursor.execute(f"select * from {table} order by Id desc LIMIT 5")
    return cursor.fetchall()

def table(jsonString) :
    return pd.DataFrame(jsonString)

